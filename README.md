# hotels-app

Run the Application using fat jar:
./gradlew build && java -jar build/libs/hotels-app-0.1.0.jar

Swagger API documentation:
http://localhost:8080/swagger-ui.html

Postman collections:
https://www.getpostman.com/collections/516d1071c3dd5bf042d3
