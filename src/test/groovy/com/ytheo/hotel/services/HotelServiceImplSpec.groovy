package com.ytheo.hotel.services

import com.ytheo.hotel.domain.Hotel
import com.ytheo.hotel.exception.ResourceNotFoundException
import com.ytheo.hotel.repositories.HotelRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.data.domain.PageRequest
import spock.lang.Specification

class HotelServiceImplSpec extends Specification {

    HotelServiceImpl service

    HotelRepository repository

    def setup() {
        repository = Mock(HotelRepository)
        service = new HotelServiceImpl(hotelRepository: repository)
    }

    def "findAllHotels works as expected"() {
        when:
            service.findAllHotels()
        then:
            1 * repository.findAll()
            0 * _
    }

    def "findAllHotels with Paging works as expected"() {
        given:
            PageRequest pageable = PageRequest.of(1, 20)
        when:
            service.findAllHotels(pageable)
        then:
            1 * repository.findAll(pageable)
            0 * _
    }

    def "findHotelById works as expected"() {
        given:
            Hotel hotel = new Hotel()
        when:
            def hotelResult = service.findHotelById(1)
        then:
            1 * repository.findById(1) >> Optional.of(hotel)
            0 * _
            hotel == hotelResult
    }

    def "findHotelById when no hotel exists"() {
        when:
            service.findHotelById(1)
        then:
            1 * repository.findById(1) >> Optional.empty()
            0 * _
            thrown(ResourceNotFoundException)
    }

    def "createHotel works as expected"() {
        given:
            Hotel hotel = new Hotel()
        when:
            service.createHotel(hotel)
        then:
            1 * repository.save(hotel)
            0 * _
    }

    def "updateHotel when hotel not found"() {
        given:
            Hotel hotel = new Hotel()
        when:
            service.updateHotel(1, hotel)
        then:
            1 * repository.existsById(1) >> false
            0 * _
            thrown(ResourceNotFoundException)
    }

    def "updateHotel works"() {
        given:
            Hotel hotel = new Hotel()
        when:
            service.updateHotel(1, hotel)
        then:
            1 * repository.existsById(1) >> true
            1 * repository.save(_ as Hotel)
            0 * _
            hotel.id == 1
    }

    def "deleteHotel works"() {
        when:
            service.deleteHotel(1)
        then:
            1 * repository.deleteById(1)
            0 * _
    }

    def "deleteHotel handles not found"() {
        when:
            service.deleteHotel(1)
        then:
            1 * repository.deleteById(1) >> {
                throw new EmptyResultDataAccessException(1)
            }
            0 * _
            thrown(ResourceNotFoundException)
    }
}
