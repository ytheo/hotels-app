package com.ytheo.hotel.mappers

import com.ytheo.hotel.controllers.dto.HotelRequestDto
import com.ytheo.hotel.domain.Hotel
import com.ytheo.hotel.domain.enumeration.StarRating
import spock.lang.Specification

class HotelMapperSpec extends Specification {

    HotelMapper mapper = new HotelMapperImpl()

    def "Apply maps to HotelResponseDto"() {
        given:
            Hotel hotel = new Hotel(id: 1, name: "Testname", address: "Address 90", starRating: StarRating.FOUR_STARS)
        when:
            def response = mapper.apply(hotel)
        then:
            with (response) {
                name == hotel.name
                address == hotel.address
                starRating == hotel.starRating.number()
            }
    }

    def "Apply maps to Hotel"() {
        given:
            HotelRequestDto requestDto = new HotelRequestDto(name: "Testname", address: "Address 90", starRating: 3)
        when:
            def hotel = mapper.apply(requestDto)
        then:
            with (hotel) {
                name == requestDto.name
                address == requestDto.address
                starRating.number() == requestDto.starRating
            }
    }
}
