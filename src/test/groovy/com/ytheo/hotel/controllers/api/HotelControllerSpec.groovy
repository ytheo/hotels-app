package com.ytheo.hotel.controllers.api

import com.ytheo.hotel.controllers.dto.HotelCollectionDto
import com.ytheo.hotel.controllers.dto.HotelRequestDto
import com.ytheo.hotel.controllers.dto.HotelResponseDto
import com.ytheo.hotel.domain.Hotel
import com.ytheo.hotel.mappers.BookingMapper
import com.ytheo.hotel.mappers.HotelMapper
import com.ytheo.hotel.services.BookingService
import com.ytheo.hotel.services.HotelService
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import spock.lang.Specification

class HotelControllerSpec extends Specification {

    HotelController controller

    def setup() {
        controller = new HotelController(Mock(HotelService), Mock(BookingService), Mock(HotelMapper), Mock(BookingMapper), 20)
    }

    def "GetHotels"() {
        given:
            Page<Hotel> page = Mock(Page)
        when:
           def result = controller.getHotels(1, 2)
        then:
            1 * controller.hotelService.findAllHotels(_ as PageRequest) >> { args ->
               def pageRequest = (PageRequest) args[0]
               pageRequest.pageNumber == 1
               pageRequest.pageSize == 2
               page
            }
            page.stream() >> [new Hotel()].stream()
            1 * controller.hotelMapper.apply(_ as Hotel) >> new HotelResponseDto()
            page.hasNext() >> true
            page.getNumber() >> 3
            result instanceof HotelCollectionDto
            result.hotels.size() == 1
            result.nextPage == 4
    }

    def "GetHotelById"() {
        given:
            Hotel hotel = new Hotel()
        when:
            controller.getHotelById(1)
        then:
            1 * controller.hotelService.findHotelById(1) >> hotel
            1 * controller.hotelMapper.apply(hotel)
    }

    def "CreateHotel"() {
        given:
            HotelRequestDto hotel = new HotelRequestDto()
        when:
            controller.createHotel(hotel)
        then:
            1 * controller.hotelMapper.apply(hotel) >> new Hotel()
            1 * controller.hotelService.createHotel(_ as Hotel)
    }

    def "UpdateHotel"() {
        given:
            HotelRequestDto hotel = new HotelRequestDto()
        when:
            controller.updateHotel(1, hotel)
        then:
            1 * controller.hotelMapper.apply(hotel) >> new Hotel()
            1 * controller.hotelService.updateHotel(1, _ as Hotel)
    }

    def "DeleteHotel"() {
        when:
            controller.deleteHotel(1)
        then:
            1 * controller.hotelService.deleteHotel(1)
    }
}
