package com.ytheo.hotel.controllers.page

import com.ytheo.hotel.exception.InvalidRequestException
import spock.lang.Specification

class PageDefaultRequestSpec extends Specification {

    def "PageDefaultRequest of works as expected"() {
        given:
            int maxSize = 20
        when:
            def pageRequest = PageDefaultRequest.of(page, size, maxSize)
        then:
            pageRequest.pageNumber == expectedPage
            pageRequest.pageSize == expectedSize
        where:
            page    | size  | expectedPage  | expectedSize
            0       | 1     | 0             | 1
            0       | 21    | 0             | 20
            null    | null  | 0             | 20
    }

    def "PageDefaultRequest handles invalid input"() {
        given:
            int maxSize = 20
        when:
            PageDefaultRequest.of(page, size, maxSize)
        then:
            thrown(InvalidRequestException)
        where:
            page    | size
            -1      | 1
            0       | -1
            0       | 0
    }

}
