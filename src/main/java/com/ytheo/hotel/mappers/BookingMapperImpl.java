package com.ytheo.hotel.mappers;

import org.springframework.stereotype.Component;

import com.ytheo.hotel.controllers.dto.BookingRequestDto;
import com.ytheo.hotel.controllers.dto.BookingResponseDto;
import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;
import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;
import com.ytheo.hotel.domain.enumeration.StarRating;

@Component
public class BookingMapperImpl implements BookingMapper {

    @Override
    public BookingResponseDto apply(Booking booking) {
    return new BookingResponseDto.Builder(booking.getId(), booking.getName(), booking.getSurname())
                .setCurrency(booking.getCurrency())
                .setPax(booking.getPax())
                .setPrice(booking.getPrice())
                .setHotelId(booking.getHotel().getId())
                .build();
    }

    @Override
    public Booking apply(BookingRequestDto bookingDto, Hotel hotel) {
        return new Booking(bookingDto.getName(),
                bookingDto.getSurname(),
                bookingDto.getPax(),
                bookingDto.getPrice(),
                bookingDto.getCurrency(),
                hotel);
    }
}
