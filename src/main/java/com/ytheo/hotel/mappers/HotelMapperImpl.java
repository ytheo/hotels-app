package com.ytheo.hotel.mappers;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;
import com.ytheo.hotel.domain.Hotel;
import com.ytheo.hotel.domain.enumeration.StarRating;

@Component
public class HotelMapperImpl implements HotelMapper {

    public HotelResponseDto apply(Hotel hotel) {
        return new HotelResponseDto.Builder(hotel.getId(), hotel.getName())
                .setAddress(hotel.getAddress())
                .setStarRating(Optional.ofNullable(hotel.getStarRating()).map(StarRating::number).orElse(null))
                .build();
    }

    public Hotel apply(HotelRequestDto hotelDto) {
        return new Hotel(hotelDto.getName(),
                hotelDto.getAddress(),
                StarRating.getByNumber(hotelDto.getStarRating()));
    }
}
