package com.ytheo.hotel.mappers;

import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;
import com.ytheo.hotel.domain.Hotel;

public interface HotelMapper {
    HotelResponseDto apply(Hotel hotel);
    Hotel apply(HotelRequestDto hotelDto);
}
