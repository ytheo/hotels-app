package com.ytheo.hotel.mappers;

import com.ytheo.hotel.controllers.dto.BookingRequestDto;
import com.ytheo.hotel.controllers.dto.BookingResponseDto;
import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;

public interface BookingMapper {

    BookingResponseDto apply(Booking booking);
    Booking apply(BookingRequestDto bookingDto, Hotel hotel);
}
