package com.ytheo.hotel.domain.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum StarRating {

    NO_STARS(0),
    ONE_STAR(1),
    TWO_STARS(2),
    THREE_STARS(3),
    FOUR_STARS(4),
    FIVE_STARS(5);

    private static final Map<Integer, StarRating> starRatingsByNumber = new HashMap<>();
    private final int number;

    static {
        for (StarRating starRating : StarRating.values()) {
            starRatingsByNumber.put(starRating.number(), starRating);
        }
    }

    StarRating(int number) {
        this.number = number;
    }

    public int number() {
        return number;
    }

    public static StarRating getByNumber(int number) {
        return starRatingsByNumber.get(number);
    }
}
