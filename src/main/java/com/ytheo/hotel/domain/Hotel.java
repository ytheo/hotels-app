package com.ytheo.hotel.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ytheo.hotel.domain.enumeration.StarRating;

@Table(name = "hotel")
@Entity
public class Hotel extends Updatable {

    @Id
    @GeneratedValue
    @Column(name = "hotel_id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "starRating")
    private StarRating starRating;

    public Hotel() {
    }

    public Hotel(Long id) {
        this.id = id;
    }

    public Hotel(String name, String address, StarRating starRating) {
        this.name = name;
        this.address = address;
        this.starRating = starRating;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public StarRating getStarRating() {
        return starRating;
    }

    public void setStarRating(StarRating starRating) {
        this.starRating = starRating;
    }

}
