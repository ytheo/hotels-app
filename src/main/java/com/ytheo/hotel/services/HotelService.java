package com.ytheo.hotel.services;

import java.util.List;

import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HotelService {

    List<Hotel> findAllHotels();
    Page<Hotel> findAllHotels(Pageable pageable);
    Hotel findHotelById(Long id);
    Hotel findOrCreateHotel(Long id);
    void createHotel(Hotel hotel);
    void updateHotel(Long id, Hotel hotel);
    void deleteHotel(Long id);
}
