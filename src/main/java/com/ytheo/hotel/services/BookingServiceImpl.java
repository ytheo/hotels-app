package com.ytheo.hotel.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.exception.ResourceNotFoundException;
import com.ytheo.hotel.exception.error.BookingError;
import com.ytheo.hotel.repositories.BookingRepository;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Override
    public List<Booking> findAllBookings() {
        return bookingRepository.findAll();
    }

    @Override
    public Page<Booking> findAllBookings(Pageable pageable) {
        return bookingRepository.findAll(pageable);
    }

    @Override
    public Booking findBookingById(Long id) {
        Optional<Booking> booking = bookingRepository.findById(id);
        return booking
                .orElseThrow(() -> new ResourceNotFoundException(BookingError.BOOKING_NOT_FOUND));
    }

    @Override
    public void createBooking(Booking booking) {
        bookingRepository.save(booking);
    }

    @Override
    public void updateBooking(Long id, Booking booking) {
        if (!bookingRepository.existsById(id)) {
            throw new ResourceNotFoundException(BookingError.BOOKING_NOT_FOUND);
        }
        booking.setId(id);
        bookingRepository.save(booking);
    }

    @Override
    public void deleteBooking(Long id) {
        try {
            bookingRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException(BookingError.BOOKING_NOT_FOUND);
        }
    }

    @Override
    public Page<Booking> findBookingsByHotelId(Long hotelId, Pageable pageable) {
        return bookingRepository.findByHotelId(hotelId, pageable);
    }
}
