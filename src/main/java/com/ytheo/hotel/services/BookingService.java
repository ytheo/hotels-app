package com.ytheo.hotel.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.ytheo.hotel.domain.Booking;

public interface BookingService {
    List<Booking> findAllBookings();
    Page<Booking> findAllBookings(Pageable pageable);
    Booking findBookingById(Long id);
    void createBooking(Booking booking);
    void updateBooking(Long id, Booking booking);
    void deleteBooking(Long id);
    Page<Booking> findBookingsByHotelId(Long hotelId, Pageable pageable);
}
