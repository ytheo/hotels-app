package com.ytheo.hotel.services;

import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;
import com.ytheo.hotel.exception.ResourceNotFoundException;
import com.ytheo.hotel.exception.error.HotelError;
import com.ytheo.hotel.repositories.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class HotelServiceImpl implements HotelService {

    @Autowired
    HotelRepository hotelRepository;

    @Override
    public List<Hotel> findAllHotels() {
        return hotelRepository.findAll();
    }

    @Override
    public Page<Hotel> findAllHotels(Pageable pageable) {
        return hotelRepository.findAll(pageable);
    }

    @Override
    public Hotel findHotelById(Long id) {
        Optional<Hotel> hotel = hotelRepository.findById(id);
        return hotel
                .orElseThrow(() -> new ResourceNotFoundException(HotelError.HOTEL_NOT_FOUND));
    }

    @Override
    public Hotel findOrCreateHotel(Long id) { //TODO change auto generated id
        Optional<Hotel> hotel = hotelRepository.findById(id);
        if (hotel.isPresent()) {
            return hotel.get();
        }
        Hotel newHotel = new Hotel(id);
        createHotel(newHotel);
        return newHotel;
    }

    @Override
    public void createHotel(Hotel hotel) {
        hotelRepository.save(hotel);
    }

    @Override
    public void updateHotel(Long id, Hotel hotel) {
        if (!hotelRepository.existsById(id)) {
            throw new ResourceNotFoundException(HotelError.HOTEL_NOT_FOUND);
        }
        hotel.setId(id);
        hotelRepository.save(hotel);
    }

    @Override
    public void deleteHotel(Long id) {
        try {
            hotelRepository.deleteById(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new ResourceNotFoundException(HotelError.HOTEL_NOT_FOUND);
        }
    }
}
