package com.ytheo.hotel.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ytheo.hotel.controllers.dto.BookingCollectionDto;
import com.ytheo.hotel.controllers.dto.BookingRequestDto;
import com.ytheo.hotel.controllers.dto.BookingResponseDto;
import com.ytheo.hotel.controllers.dto.HotelCollectionDto;
import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;

import io.swagger.annotations.ApiOperation;

@RequestMapping(path = "/api/bookings")
public interface BookingApi {

    @ApiOperation(value = "Retrieve all bookings", notes = "Retrieving the collection of bookings")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    BookingCollectionDto getBookings(@RequestParam(value = "page") Integer page,
                                     @RequestParam(value = "size", required = false) Integer pageSize); // TODO Paging class

    @ApiOperation(value = "Retrieve a booking by id", notes = "Retrieving a booking by id")
    @GetMapping(value = "/(id}", produces = MediaType.APPLICATION_JSON_VALUE)
    BookingResponseDto getBookingById(@PathVariable("id") Long id);

    @ApiOperation(value = "Create a new booking", notes = "Creating a new booking")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    void createBooking(@RequestBody BookingRequestDto booking);

    @ApiOperation(value = "Update an existing booking", notes = "Update an existing booking")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    void updateBooking(@PathVariable("id") Long id, @RequestBody BookingRequestDto booking);

    @ApiOperation(value = "Delete an existing booking", notes = "Delete an existing booking")
    @DeleteMapping(value = "/{id}")
    void deleteBooking(@PathVariable("id") Long id);
}
