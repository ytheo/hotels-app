package com.ytheo.hotel.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ytheo.hotel.controllers.dto.BookingCollectionDto;
import com.ytheo.hotel.controllers.dto.HotelCollectionDto;
import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;

import io.swagger.annotations.ApiOperation;

@RequestMapping(path = "/api/hotels")
public interface HotelApi {

    @ApiOperation(value = "Retrieve all hotels", notes = "Retrieving the collection of hotels")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    HotelCollectionDto getHotels(@RequestParam(value="page") Integer page,
                                 @RequestParam(value="size", required = false) Integer pageSize);

    @ApiOperation(value = "Retrieve a hotel by id", notes = "Retrieving a hotel by id")
    @GetMapping(value = "/(id}", produces = MediaType.APPLICATION_JSON_VALUE)
    HotelResponseDto getHotelById(@PathVariable("id") Long id);

    @ApiOperation(value = "Create a new hotel", notes = "Creating a new hotel")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    void createHotel(@RequestBody HotelRequestDto hotel);

    @ApiOperation(value = "Update an existing hotel", notes = "Update an existing hotel")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    void updateHotel(@PathVariable( "id" ) Long id, @RequestBody HotelRequestDto hotel);

    @ApiOperation(value = "Delete an existing hotel", notes = "Delete an existing hotel")
    @DeleteMapping(value = "/{id}")
    void deleteHotel(@PathVariable("id") Long id);

    @ApiOperation(value = "Retrieve all hotel bookings", notes = "Retrieving all hotel bookings")
    @GetMapping(value = "/{id}/bookings", produces = MediaType.APPLICATION_JSON_VALUE)
    BookingCollectionDto getHotelBookings(@PathVariable("id") Long id,
                                          @RequestParam(value="page") Integer page,
                                          @RequestParam(value="size", required = false) Integer pageSize);
}
