package com.ytheo.hotel.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ytheo.hotel.domain.Booking;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    Page<Booking> findByHotelId(Long hotelId, Pageable pageable);


}
