package com.ytheo.hotel.exception.error;

public enum HotelError {

    HOTEL_NOT_FOUND("Hotel not found");;

    private String description;

    HotelError(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
