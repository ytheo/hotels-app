package com.ytheo.hotel.exception.error;

public enum BookingError {

    BOOKING_NOT_FOUND("Booking not found");;

    private String description;

    BookingError(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
