package com.ytheo.hotel.exception.error;

public enum PagingRequestError {

    INVALID_PAGE("Page parameter is invalid."),
    INVALID_PAGE_SIZE("Page size parameter is invalid.");

    private String description;

    PagingRequestError(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
