package com.ytheo.hotel.exception.error;

public enum ResourceError {

    RESOURCE_NOT_FOUND("Resource not found");;

    private String description;

    ResourceError(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }
}
