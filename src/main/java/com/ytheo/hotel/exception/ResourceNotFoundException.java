package com.ytheo.hotel.exception;

public class ResourceNotFoundException extends PlatformException {
    public ResourceNotFoundException(Enum error) {
        super(error);
    }
}
