package com.ytheo.hotel.exception;

public class InvalidRequestException extends PlatformException {

    public InvalidRequestException(Enum error) {
        super(error);
    }

}
