package com.ytheo.hotel.exception;

public class PlatformException extends RuntimeException {

    public PlatformException(Enum error) {
        super(error.name());
    }

}
