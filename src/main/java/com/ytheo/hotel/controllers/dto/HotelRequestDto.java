package com.ytheo.hotel.controllers.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class HotelRequestDto {

    @NotEmpty(message = "Hotel name should not be empty")
    private String name;

    @NotEmpty(message = "Hotel address should not be empty")
    private String address;

    @NotNull(message = "Hotel starRating should not be null")
    @Min(value = 0, message = "Star Rating should not be less than 0")
    @Max(value = 5, message = "Star Rating should not be greater than 5")
    private Integer starRating;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStarRating() {
        return starRating;
    }

    public void setStarRating(Integer starRating) {
        this.starRating = starRating;
    }

}

