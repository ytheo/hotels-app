package com.ytheo.hotel.controllers.dto;

import java.util.List;

public class BookingCollectionDto {

    private List<BookingResponseDto> bookings;
    private Integer nextPage;

    public BookingCollectionDto(List<BookingResponseDto> bookings, Integer nextPage) {
        this.bookings = bookings;
        this.nextPage = nextPage;
    }

    public List<BookingResponseDto> getBookings() {
        return bookings;
    }

    public void setBookings(List<BookingResponseDto> bookings) {
        this.bookings = bookings;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }
}
