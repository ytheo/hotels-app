package com.ytheo.hotel.controllers.dto;

public class HotelResponseDto {

    private Long id;
    private String name;
    private String address;
    private Integer starRating;

    public HotelResponseDto() {
    }

    private HotelResponseDto(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.address = builder.address;
        this.starRating = builder.starRating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStarRating() {
        return starRating;
    }

    public void setStarRating(Integer starRating) {
        this.starRating = starRating;
    }

    public static class Builder {

        private long id;
        private String name;
        private String address;
        private Integer starRating;

        public Builder(long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setStarRating(Integer starRating) {
            this.starRating = starRating;
            return this;
        }

        public HotelResponseDto build() {
            return new HotelResponseDto(this);
        }
    }
}

