package com.ytheo.hotel.controllers.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class BookingRequestDto {

    @NotEmpty(message = "Booking name should not be empty")
    private String name;

    @NotEmpty(message = "Booking surname should not be empty")
    private String surname;

    @NotNull(message = "Booking PAX number should not be null")
    @Min(value = 0, message = "Booking PAX number should not be less than 0")
    private Integer pax;

    @NotNull(message = "Booking price should not be null")
    private BigDecimal price;

    @NotEmpty(message = "Booking currency should not be empty")
    private String currency;

    @NotNull(message = "Booking should be associated with a hotel id")
    private Long hotelId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }
}

