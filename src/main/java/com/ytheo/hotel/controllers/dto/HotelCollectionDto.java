package com.ytheo.hotel.controllers.dto;

import java.util.List;

public class HotelCollectionDto { //TODO Generic PagedCollection

    private List<HotelResponseDto> hotels;
    private Integer nextPage;

    public HotelCollectionDto(List<HotelResponseDto> hotels, Integer nextPage) {
        this.hotels = hotels;
        this.nextPage = nextPage;
    }

    public List<HotelResponseDto> getHotels() {
        return hotels;
    }

    public void setHotels(List<HotelResponseDto> hotels) {
        this.hotels = hotels;
    }

    public Integer getNextPage() {
        return nextPage;
    }

    public void setNextPage(Integer nextPage) {
        this.nextPage = nextPage;
    }
}
