package com.ytheo.hotel.controllers.dto;

import java.math.BigDecimal;

public class BookingResponseDto {

    private long id;
    private String name;
    private String surname;
    private Integer pax;
    private BigDecimal price;
    private String currency;
    private Long hotelId;

    public BookingResponseDto() {
    }

    private BookingResponseDto(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.surname = builder.surname;
        this.pax = builder.pax;
        this.price = builder.price;
        this.currency = currency;
        this.hotelId = builder.hotelId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getPax() {
        return pax;
    }

    public void setPax(Integer pax) {
        this.pax = pax;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getHotelId() {
        return hotelId;
    }

    public void setHotelId(Long hotelId) {
        this.hotelId = hotelId;
    }

    public static class Builder {

        private long id;
        private String name;
        private String surname;
        private Integer pax;
        private BigDecimal price;
        private String currency;
        private Long hotelId;

        public Builder(long id, String name, String surname) {
            this.id = id;
            this.name = name;
            this.surname = surname;
        }

        public BookingResponseDto.Builder setPax(Integer pax) {
            this.pax = pax;
            return this;
        }

        public BookingResponseDto.Builder setPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public BookingResponseDto.Builder setCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public BookingResponseDto.Builder setHotelId(Long hotelId) {
            this.hotelId = hotelId;
            return this;
        }

        public BookingResponseDto build() {
            return new BookingResponseDto(this);
        }
    }
}
