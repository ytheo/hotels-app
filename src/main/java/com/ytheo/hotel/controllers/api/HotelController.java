package com.ytheo.hotel.controllers.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ytheo.hotel.api.HotelApi;
import com.ytheo.hotel.controllers.dto.BookingCollectionDto;
import com.ytheo.hotel.controllers.dto.BookingResponseDto;
import com.ytheo.hotel.controllers.dto.HotelCollectionDto;
import com.ytheo.hotel.controllers.dto.HotelRequestDto;
import com.ytheo.hotel.controllers.dto.HotelResponseDto;
import com.ytheo.hotel.controllers.page.PageDefaultRequest;
import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;
import com.ytheo.hotel.mappers.BookingMapper;
import com.ytheo.hotel.mappers.HotelMapper;
import com.ytheo.hotel.services.BookingService;
import com.ytheo.hotel.services.HotelService;

@RestController
public class HotelController implements HotelApi {

    private static final Integer NO_PAGE = -1;

    private final int maxPagingSize;
    private final HotelService hotelService;
    private final BookingService bookingService;
    private final HotelMapper hotelMapper;
    private final BookingMapper bookingMapper;

    public HotelController(@Autowired HotelService hotelService,
                           @Autowired BookingService bookingService,
                           @Autowired HotelMapper hotelMapper,
                           @Autowired BookingMapper bookingMapper,
                           @Value("${hotels.page.maxSize}") int maxSize) {
        this.hotelService = hotelService;
        this.bookingService = bookingService;
        this.hotelMapper = hotelMapper;
        this.bookingMapper = bookingMapper;
        this.maxPagingSize = maxSize;
    }

    @Override
    public HotelCollectionDto getHotels(Integer page, Integer pageSize) {
        Page<Hotel> hotelPage = hotelService.findAllHotels(PageDefaultRequest.of(page, pageSize, maxPagingSize));

        List<HotelResponseDto> hotels = hotelPage.stream()
                                    .map(hotelMapper::apply)
                                    .collect(Collectors.toList());
        Integer nextPage = hotelPage.hasNext() ? hotelPage.getNumber() + 1: NO_PAGE;
        return new HotelCollectionDto(hotels, nextPage);
    }

    @Override
    public HotelResponseDto getHotelById(@PathVariable("id") Long id) {
        return hotelMapper.apply(hotelService.findHotelById(id));
    }

    @Override
    public void createHotel(@RequestBody @Valid HotelRequestDto hotel) {
        hotelService.createHotel(hotelMapper.apply(hotel));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void updateHotel(@PathVariable("id") Long id, @RequestBody @Valid HotelRequestDto hotel) {
        hotelService.updateHotel(id, hotelMapper.apply(hotel));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void deleteHotel(@PathVariable("id") Long id) {
        hotelService.deleteHotel(id);
    }

    @Override
    public BookingCollectionDto getHotelBookings(@PathVariable("id") Long id, Integer page, Integer pageSize) {
        Page<Booking> bookingsPage = bookingService.findBookingsByHotelId(id, PageDefaultRequest.of(page, pageSize, maxPagingSize));

        List<BookingResponseDto> bookings = bookingsPage.stream()
                .map(bookingMapper::apply)
                .collect(Collectors.toList());
        Integer nextPage = bookingsPage.hasNext() ? bookingsPage.getNumber() + 1: NO_PAGE;
        return new BookingCollectionDto(bookings, nextPage);
    }

}
