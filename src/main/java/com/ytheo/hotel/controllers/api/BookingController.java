package com.ytheo.hotel.controllers.api;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ytheo.hotel.api.BookingApi;
import com.ytheo.hotel.controllers.dto.BookingCollectionDto;
import com.ytheo.hotel.controllers.dto.BookingRequestDto;
import com.ytheo.hotel.controllers.dto.BookingResponseDto;
import com.ytheo.hotel.controllers.page.PageDefaultRequest;
import com.ytheo.hotel.domain.Booking;
import com.ytheo.hotel.domain.Hotel;
import com.ytheo.hotel.mappers.BookingMapper;
import com.ytheo.hotel.services.BookingService;
import com.ytheo.hotel.services.HotelService;

@RestController
public class BookingController implements BookingApi {

    private static final Integer NO_PAGE = -1;

    private final int maxPagingSize; //TODO  new  for booking
    private final BookingService bookingService;
    private final HotelService hotelService;
    private final BookingMapper bookingMapper;

    public BookingController(@Autowired BookingService bookingService,
                             @Autowired HotelService hotelService,
                             @Autowired BookingMapper bookingMapper,
                             @Value("${hotels.page.maxSize}") int maxSize) {
        this.bookingService = bookingService;
        this.hotelService = hotelService;
        this.bookingMapper = bookingMapper;
        this.maxPagingSize = maxSize;
    }

    @Override
    public BookingCollectionDto getBookings(Integer page, Integer pageSize) {
        
        Page<Booking> bookingPage = bookingService.findAllBookings(PageDefaultRequest.of(page, pageSize, maxPagingSize));

        List<BookingResponseDto> bookings = bookingPage.stream()
                                    .map(bookingMapper::apply)
                                    .collect(Collectors.toList());
        Integer nextPage = bookingPage.hasNext() ? bookingPage.getNumber() + 1: NO_PAGE;
        return new BookingCollectionDto(bookings, nextPage);
    }

    @Override
    public BookingResponseDto getBookingById(Long id) {
        return bookingMapper.apply(bookingService.findBookingById(id));
    }

    @Transactional
    @Override
    public void createBooking(@RequestBody @Valid BookingRequestDto booking) {
        Hotel hotel = hotelService.findHotelById(booking.getHotelId());
        bookingService.createBooking(bookingMapper.apply(booking, hotel));

    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void updateBooking(Long id, @RequestBody @Valid BookingRequestDto booking) {
        Hotel hotel = hotelService.findHotelById(booking.getHotelId());
        bookingService.updateBooking(id, bookingMapper.apply(booking, hotel));
    }

    @Override
    @ResponseStatus(HttpStatus.OK)
    public void deleteBooking(Long id) {
        bookingService.deleteBooking(id);
    }

}
