package com.ytheo.hotel.controllers.page;

import java.util.Optional;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.ytheo.hotel.exception.InvalidRequestException;
import com.ytheo.hotel.exception.error.HotelError;
import com.ytheo.hotel.exception.error.PagingRequestError;

public class PageDefaultRequest {

    public static PageRequest of(Integer page, Integer size, Integer maxSize) {
        return PageRequest.of(resolvePage(page), resolveSize(size, maxSize));
    }

    private static int resolvePage(Integer page) {
        if (!Optional.ofNullable(page).isPresent()) {
            return 0;
        }
        if (page < 0) {
            throw new InvalidRequestException(PagingRequestError.INVALID_PAGE);
        }
        return page;
    }

    private static int resolveSize(Integer size, Integer maxSize) {
        if (!Optional.ofNullable(size).isPresent()) {
            return maxSize;
        }
        if (size <= 0) {
            throw new InvalidRequestException(PagingRequestError.INVALID_PAGE_SIZE);
        }
        return (size <= maxSize) ? size : maxSize;
    }

}
